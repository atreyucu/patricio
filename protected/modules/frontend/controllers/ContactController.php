<?php

class ContactController extends Controller
{
    public $common_data;

    public function init()
    {
        $this->layout = '/layout/main';
        $this->common_data = $this->getVariablesArray();
    }

    private function getVariablesArray()
    {
        $header = GeneralHeaderInformation::model()->find();
        $main_menu = MainNavegationMenu::model()->find();
        $social_links = SocialNavegationMenu::model()->find();
        $footer_menu = FooterMenu::model()->find();
        $copy_right = FooterCopyright::model()->find();
        $resume = FooterResume::model()->find();
        $usefullinks = FooterUsefulLinks::model()->findAll();
        $catalog = FooterCatalog::model()->findAll();

        $site_logo = '';
        if(isset($header))
        {
            $site_logo = $header->_site_logo->getFileUrl('normal');
        }

        return array(
            'site_logo' => $site_logo,
            'main_menu' => $main_menu,
            'social_links' => $social_links,
            'footer_menu' => $footer_menu,
            'copy_right' => $copy_right,
            'resume' => $resume,
            'usefullinks' =>$usefullinks,
            'catalog' => $catalog,
        );
    }

    public function  actionContact(){

        $contact_page = ContactUs::model()->find();

        $title = "";
        $sub_title = "";
        $phone_title = "";
        $phone = "";
        $email_title = "";
        $email = "";
        $description = "";
        $image = "";

        if(isset($contact_page))
        {
            $title = $contact_page->title;
            $sub_title = $contact_page->sub_title;
            $phone_title = $contact_page->phone_title;
            $phone = $contact_page->phone;
            $email_title = $contact_page->email_title;
            $email = $contact_page->email;
            $description = $contact_page->description;

            if(isset($contact_page->_image))
            {
                $image = $contact_page->_image->getFileUrl('normal');
            }
        }


        $this->render('contact',array(
                        "title" => $title,
                        "sub_title" => $sub_title,
                        "phone_title" => $phone_title,
                        "phone" => $phone,
                        "email_title" => $email_title,
                        "email" => $email,
                        "description" => $description,
                        "image" => $image,
        ));
    }

    public function actionSendEmail()
    {
        if(Yii::app()->request->isPostRequest){
            $customer_name= $_POST['name'];
            $customer_email = $_POST['email'];
//            $customer_phone = $_POST['phone'];
            $customer_comment = $_POST['message'];
//            $customer_org = $_POST['company-org'];
//            $company = CompanyInfo::model()->find();
            $contact = ContactUs::model()->find();

            $to = "";
            if(isset($contact))
            {
                $to = $contact->email;
            }

            $subject = 'Contact Us';

            $reply = $customer_email;
            $headers = "From: {$customer_email}" .
                "\r\nReply-To: {$reply}" .
                "\r\nContent-type: text/html; charset=utf-8";

            $body = 'Name: '. $customer_name;
            $body = $body.'<br> Email Address: '.$customer_email;
//            $body = $body. '<br> Phone Number: '.$customer_phone;
//            $body = $body. '<br> Customer Comapany/Organization: '.$customer_org;
            $body = $body. '<br> Customer Comment: ' . $customer_comment;

            if(@mail($to, $subject, $body, $headers)){
                Yii::app()->user->setFlash('success', Yii::t('flash','Thanks you for contact us. We answer you as soon as possible.'));
                $this->redirect('/contact');
            }
            else{
                Yii::app()->user->setFlash('danger', Yii::t('flash','Sorry, we have a problem with the server, try again in a minutes.'));
                $this->redirect('/contact');
            }
        }
        else
            $this->redirect('/contact');
    }

    public function getSeoUrl($url)
    {

        $urlRule = UrlRule::model()->find('route=:route', array(':route' => $url));
        if (isset($urlRule)) {
            $seo_url = $urlRule->seo_url;
            return $seo_url;
        }
        return $url;
    }
}

?>