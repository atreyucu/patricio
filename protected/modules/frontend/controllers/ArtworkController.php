<?php

class ArtworkController extends Controller
{
    public $common_data;

    public function init()
        {
            $this->layout = '/layout/main';
            $this->common_data = $this->getVariablesArray();
        }

    private function getVariablesArray()
    {
        $header = GeneralHeaderInformation::model()->find();
        $main_menu = MainNavegationMenu::model()->find();
        $social_links = SocialNavegationMenu::model()->find();
        $footer_menu = FooterMenu::model()->find();
        $copy_right = FooterCopyright::model()->find();
        $resume = FooterResume::model()->find();
        $usefullinks = FooterUsefulLinks::model()->findAll();
        $catalog = FooterCatalog::model()->findAll();

        $site_logo = '';
        if(isset($header))
        {
            $site_logo = $header->_site_logo->getFileUrl('normal');
        }

        return array(
            'site_logo' => $site_logo,
            'main_menu' => $main_menu,
            'social_links' => $social_links,
            'footer_menu' => $footer_menu,
            'copy_right' => $copy_right,
            'resume' => $resume,
            'usefullinks' =>$usefullinks,
            'catalog' => $catalog,
        );
    }

    public function  actionArtwork(){
        $artwork_main = ArtworkMain::model()->find();
        $top_filters = ArtworkTopFilter::model()->find();
        $bottom_filters = ArtworkBottomFilter::model()->find();

        $gall_criteria = new CDbCriteria();

        $session = Yii::app()->session;

        if(isset($_GET['country']) || isset($_GET['tech'])){
            $country = isset($_GET['country'])?$_GET['country']:-1;
            $technique = isset($_GET['tech'])?$_GET['tech']:-1;


            if($country == -1){
                //$country = isset($_SESSION['filters_options']['country'])?$_SESSION['filters_options']['country']:-1;
                $country = $session['country'];
            }
            else{
                //$_SESSION['filters_options']['country']= $country;
                $session['country'] = $country;
            }

            if($technique == -1){
                //$technique = isset($_SESSION['filters_options']['tech'])?$_SESSION['filters_options']['tech']:-1;
                $technique = $session['tech'];
            }
            else{
                //$_SESSION['filters_options']['tech'] = $technique;
                $session['tech'] = $technique;
            }

            $gall_criteria->order='show_order';

            if($country != -1 && $technique != -1 && $country != 4 && $technique != 4){
                $gall_criteria->condition='(country=:country AND art_technique=:tech)';
                $gall_criteria->params=array(':country'=>$country,':tech'=>$technique);
            }
            elseif($country != -1 && $country != 4){
                $gall_criteria->condition='(country=:country)';
                $gall_criteria->params=array(':country'=>$country);
            }
            elseif($technique != -1 && $technique != 4){
                $gall_criteria->condition='(art_technique=:tech)';
                $gall_criteria->params=array(':tech'=>$technique);
            }
        }
        else{
            $country = isset($session['country'])?$session['country']:4;
            $technique = isset($session['tech'])?$session['tech']:4;

            $gall_criteria->order='show_order';

            if($country != -1 && $technique != -1 && $country != 4 && $technique != 4){
                $gall_criteria->condition='(country=:country AND art_technique=:tech)';
                $gall_criteria->params=array(':country'=>$country,':tech'=>$technique);
            }
            elseif($country != -1 && $country != 4){
                $gall_criteria->condition='(country=:country)';
                $gall_criteria->params=array(':country'=>$country);
            }
            elseif($technique != -1 && $technique != 4){
                $gall_criteria->condition='(art_technique=:tech)';
                $gall_criteria->params=array(':tech'=>$technique);
            }
        }

        if($country == 4){
            $country_active = '-1';
        }
        else{
            $country_active = $country;
        }

        if($technique == 4){
            $tech_active = '-1';
        }
        else{
            $tech_active = $technique;
        }

        $galleries = ArtworkGallery::model()->findAll($gall_criteria);

        $this->render('artwork',array(
            'main'=>$artwork_main,
            'top_filter'=>$top_filters,
            'bottom_filter'=>$bottom_filters,
            'galleries'=>$galleries,
            'country'=>$country_active,
            'tech'=>$tech_active
        ));
    }

    public function getSeoUrl($url)
    {

        $urlRule = UrlRule::model()->find('route=:route', array(':route' => $url));
        if (isset($urlRule)) {
            $seo_url = $urlRule->seo_url;
            return $seo_url;
        }
        return $url;
    }
}

?>