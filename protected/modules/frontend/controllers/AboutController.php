<?php

class AboutController extends Controller
{
    public $common_data;

    public function init()
    {
        $this->layout = '/layout/main';
        $this->common_data = $this->getVariablesArray();
    }

    private function getVariablesArray()
    {
        $header = GeneralHeaderInformation::model()->find();
        $main_menu = MainNavegationMenu::model()->find();
        $social_links = SocialNavegationMenu::model()->find();
        $footer_menu = FooterMenu::model()->find();
        $copy_right = FooterCopyright::model()->find();
        $resume = FooterResume::model()->find();
        $usefullinks = FooterUsefulLinks::model()->findAll();
        $catalog = FooterCatalog::model()->findAll();

        $site_logo = '';
        if(isset($header))
        {
            $site_logo = $header->_site_logo->getFileUrl('normal');
        }

        return array(
            'site_logo' => $site_logo,
            'main_menu' => $main_menu,
            'social_links' => $social_links,
            'footer_menu' => $footer_menu,
            'copy_right' => $copy_right,
            'resume' => $resume,
            'usefullinks' =>$usefullinks,
            'catalog' => $catalog,
        );
    }

    public function  actionAbout(){
        $about_page = AboutContent::model()->find();
        $title = '';
        $sub_title = '';
        $long_description = '';
        $main_image = '';
        $tab1_label = '';
        $tab2_label = '';
        $tab2_link = '';
        $tab3_label = '';

        if(isset($about_page))
        {

            $title = $about_page->title;
            $sub_title = $about_page->sub_title;
            $long_description = $about_page->long_description;
            if(isset($about_page->_main_image))
            {
                $main_image = $about_page->_main_image->getFileUrl('normal');
            }
            $tab1_label = $about_page->tab1_label;
            $tab2_label = $about_page->tab2_label;
            $tab3_label = $about_page->tab3_label;
            if($about_page->tab2_link != null)
            {
                $tab2_link = $about_page->tab2_link;
            }
            if($about_page->tab3_link != null)
            {
                $tab3_link = $about_page->tab3_link;
            }

        }
        $resume_file = '';
        $footer_resume = FooterResume::model()->find();
        if(isset($footer_resume))
        {
            $resume_file = '/uploads/file_'.$footer_resume->id.'.pdf';
        }
        $this->render('about',array(
                    'title' => $title,
                    'sub_title' => $sub_title,
                    'long_description' => $long_description,
                    'main_image' => $main_image,

                    'tab1_label' => $tab1_label,
                    'tab1_document' => $resume_file,
                    'tab2_label' => $tab2_label,
                    'tab2_link' => $tab2_link,
                    'tab3_label' => $tab3_label,
                    'tab3_link' => $tab3_link,
        ));
    }

    public function getSeoUrl($url)
    {

        $urlRule = UrlRule::model()->find('route=:route', array(':route' => $url));
        if (isset($urlRule)) {
            $seo_url = $urlRule->seo_url;
            return $seo_url;
        }
        return $url;
    }
}

?>