<?php

class DefaultController extends Controller
{
    public $common_data;

    public function init()
    {
        $this->layout = '/layout/main';
        $this->common_data = $this->getVariablesArray();
    }

    private function getVariablesArray()
    {
        $header = GeneralHeaderInformation::model()->find();
        $main_menu = MainNavegationMenu::model()->find();
        $social_links = SocialNavegationMenu::model()->find();
        $footer_menu = FooterMenu::model()->find();
        $copy_right = FooterCopyright::model()->find();
        $resume = FooterResume::model()->find();
        $usefullinks = FooterUsefulLinks::model()->findAll();
        $catalog = FooterCatalog::model()->findAll();

        $site_logo = '';
        if(isset($header))
        {
            $site_logo = $header->_site_logo->getFileUrl('normal');
        }

        return array(
            'site_logo' => $site_logo,
            'main_menu' => $main_menu,
            'social_links' => $social_links,
            'footer_menu' => $footer_menu,
            'copy_right' => $copy_right,
            'resume' => $resume,
            'usefullinks' =>$usefullinks,
            'catalog' => $catalog,
        );
    }

    public function  actionHome(){
        //$this->layout = '/layout/main';
        $home_page = HomePage::model()->findAll();
        $home_data = array();
        foreach($home_page as $h)
        {
            $image = null;
            if(isset($h->_image))
            {
                $image = $h->_image->getFileUrl('normal');
            }
            $title = '';
            if($h->title != null)
            {
                $title = $h->title;
            }
            $sub_title = '';
            if($h->sub_title != null)
            {
                $sub_title = $h->sub_title;
            }
            $home_data[] = array(
                    'image' => $image,
                    'title' => $title,
                    'sub_title' => $sub_title,
                    'banner_link' => $h->banner_link,
            );
        }
        $this->render('home',array(
                    'home_data' => $home_data
        ));
    }

    public function getSeoUrl($url)
    {

        $urlRule = UrlRule::model()->find('route=:route', array(':route' => $url));
        if (isset($urlRule)) {
            $seo_url = $urlRule->seo_url;
            return $seo_url;
        }
        return $url;
    }
}

?>