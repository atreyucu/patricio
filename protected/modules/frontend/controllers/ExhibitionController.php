<?php

class ExhibitionController extends Controller
{
    public $common_data;

    public function init()
    {
        $this->layout = '/layout/main';
        $this->common_data = $this->getVariablesArray();
    }

    private function getVariablesArray()
    {
        $header = GeneralHeaderInformation::model()->find();
        $main_menu = MainNavegationMenu::model()->find();
        $social_links = SocialNavegationMenu::model()->find();
        $footer_menu = FooterMenu::model()->find();
        $copy_right = FooterCopyright::model()->find();
        $resume = FooterResume::model()->find();
        $usefullinks = FooterUsefulLinks::model()->findAll();
        $catalog = FooterCatalog::model()->findAll();
        $site_logo = '';
        if(isset($header))
        {
            $site_logo = $header->_site_logo->getFileUrl('normal');
        }

        return array(
            'site_logo' => $site_logo,
            'main_menu' => $main_menu,
            'social_links' => $social_links,
            'footer_menu' => $footer_menu,
            'copy_right' => $copy_right,
            'resume' => $resume,
            'usefullinks' =>$usefullinks,
            'catalog' => $catalog,
        );
    }

    public function  actionExhibition(){
        $section1 = ExhibitionSection1::model()->find();
        $section2 = ExhibitionSection2::model()->find();
        $this->render('exhibition',array(
            'section1'=>$section1,
            'section2'=>$section2,
        ));
    }

    public function getSeoUrl($url)
    {

        $urlRule = UrlRule::model()->find('route=:route', array(':route' => $url));
        if (isset($urlRule)) {
            $seo_url = $urlRule->seo_url;
            return $seo_url;
        }
        return $url;
    }
}

?>