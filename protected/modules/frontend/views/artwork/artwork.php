<div class="left-bar"><a href="/exhibitions" class="btn btn-default"><i class="fa fa-angle-up fa-3x"></i></a><a href="/contact" class="btn btn-default"><i class="fa fa-angle-down fa-3x"></i></a></div>
<div class="right-bar"><i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle active"></i><i class="fa fa-circle"></i>
</div>
<div class="content artwork">
    <div class="main">
        <div class="artwork">
            <div class="main-title">
                <h1><?php echo $main->title?></h1>
                <h2><?php echo $main->sub_title?></h2>
            </div>
            <div class="artwork-filters">
                <ul class="styles">
                    <li <?php echo $tech=='0'?'class="active"':''?>><a class="btn btn-default" href="/artwork?tech=0"><?php echo $top_filter->tab1_text?><br class="visible-xs"></a></li>
                    <li <?php echo $tech=='1'?'class="active"':''?>><a class="btn btn-default" href="/artwork?tech=1"><?php echo $top_filter->tab2_text?> <br class="visible-xs"></a></li>
                    <li <?php echo $tech=='2'?'class="active"':''?>><a class="btn btn-default" href="/artwork?tech=2"><?php echo $top_filter->tab3_text?> <br class="visible-xs"></a></li>
                    <li <?php echo $tech=='-1'?'class="active"':''?>><a class="btn btn-default" href="/artwork?tech=4">All <br class="visible-xs"></a></li>
                </ul>
                <ul>
                    <li <?php echo $country=='0'?'class="active"':''?>><a class="btn btn-default" href="/artwork?country=0"><?php echo $bottom_filter->tab1_text?></a></li>
                    <li <?php echo $country=='1'?'class="active"':''?>><a class="btn btn-default" href="/artwork?country=1"><?php echo $bottom_filter->tab2_text?></a></li>
                    <li <?php echo $country=='2'?'class="active"':''?>><a class="btn btn-default" href="/artwork?country=2"><?php echo $bottom_filter->tab3_text?></a></li>
                    <li <?php echo $country=='-1'?'class="active"':''?>><a class="btn btn-default" href="/artwork?country=4">All</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
            <div class="artwork-imgs">
                <?php foreach($galleries as $gall):?>
                   <img id="<?php echo $gall->id?>" src="<?php echo $gall->_listing_image->getFileUrl('normal')?>" class="artwork-img"/>
                   <div id="info-hidden1-<?php echo $gall->id?>" style="display: none">
                       <div class="desc"><a class="btn close"><i class="fa fa-times"></i></a>
                           <div class="inner-wrapper">
                               <h2><?php echo $gall->title?></h2>
                               <h3><?php echo $gall->short_description?></h3>
                               <div class="dimensions"><?php echo $gall->dimension_text?></div>
                               <div class="autor">By <b><?php echo $gall->author_name?></b></div>
                               <div class="text">
                                   <?php echo $gall->long_description?>
                               </div>
                           </div><a class="btn btn-default buy" href="<?php echo $gall->popup_button_link?>"><?php echo $gall->popup_button_label?></a>
                       </div>
                   </div>
                    <div id="info-hidden2-<?php echo $gall->id?>" style="display: none">
                        <div class="desc">
                            <h2><?php echo $gall->title?></h2>
                            <h3><?php echo $gall->short_description?></h3>
                            <div class="dimensions">4<?php echo $gall->dimension_text?></div>
                            <div class="autor">By <b><?php echo $gall->author_name?></b></div>
                            <?php echo $gall->long_description?>
                            <a class="btn btn-default buy" href="<?php echo $gall->popup_button_link?>"><?php echo $gall->popup_button_label?></a>
                        </div>
                    </div>
                <?php endforeach;?>
             </div>
            <div id="artwork-modal">
                <div class="modal-wrapper"><img src="/static/img/artwork-3.jpg"/>
                    <div id="popup1-content">
                        <div class="desc"><a class="btn close"><i class="fa fa-times"></i></a>
                            <div class="inner-wrapper">
                                <h2>TITLE</h2>
                                <h3>Short Description</h3>
                                <div class="dimensions">40 X 28 INCHES ( 70 CM X 100 CM)</div>
                                <div class="autor">By <b>PATRICIO RODRIGUEZ</b></div>
                                <div class="text">
                                    <p>Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
                                </div>
                            </div><a class="btn btn-default buy">PURCHASE</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="artwork-small-modal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <button data-dismiss="modal" class="btn close"><i class="fa fa-times"></i></button>
                            <div style="background-imgage: url(/static/img/artwork-3.jpg)" class="img hidden-xs"></div><img src="/static/img/artwork-3.jpg" class="visible-xs"/>
                            <div class="wrapper">
                                <div class="desc">
                                    <h2>TITLE</h2>
                                    <h3>Short Description</h3>
                                    <div class="dimensions">40 X 28 INCHES ( 70 CM X 100 CM)</div>
                                    <div class="autor">By <b>PATRICIO RODRIGUEZ</b></div>
                                    <p>Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p><a class="btn btn-default buy">PURCHASE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>