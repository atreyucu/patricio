<div class="left-bar"><a href="/about" class="btn btn-default"><i class="fa fa-angle-up fa-3x"></i></a><a href="/artwork" class="btn btn-default"><i class="fa fa-angle-down fa-3x"></i></a></div>
<div class="right-bar"><i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle active"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i>
</div>
<div class="content exhibitions">
    <div class="main">
        <div class="exhibitions">
            <div id="main-title-section1" class="main-title">
                <h1><?php echo $section1->section1_title?></h1>
                <h2><?php echo $section1->section1_subtitle?></h2>
            </div>

            <div id="main-title-section2" class="main-title" style="display: none">
                <h1><?php echo $section2->section2_title?></h1>
                <h2><?php echo $section2->section2_subtitle?></h2>
            </div>
            <div class="exhibitions-selector">
                <ul>
                    <li class="active"><a href="#exhibitions" data-toggle="tab" class="btn btn-default" onclick="exibition_change_title1()"><?php echo $section1->filter_label?></a></li>
                    <li><a href="#studies" data-toggle="tab" class="btn btn-default" onclick="exibition_change_title2()"><?php echo $section2->filter_label?></a></li>
                </ul>
            </div>
            <div class="exhibitions-text tab-content">
                <div id="exhibitions" class="tab-pane fade in active">
                   <?php echo $section1->section1_long_description?>
                </div>
                <div id="studies" class="tab-pane fade">
                    <?php echo $section2->section2_long_description?>
                </div>
            </div>
        </div>
    </div>
</div>