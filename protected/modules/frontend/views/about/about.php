<div class="left-bar"><a href="/" class="btn btn-default"><i class="fa fa-angle-up fa-3x"></i></a>
    <a href="<?php echo $this->createUrl("/exhibitions")?>" class="btn btn-default"><i class="fa fa-angle-down fa-3x"></i></a></div>
<div class="right-bar"><i class="fa fa-circle"></i><i class="fa fa-circle active"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i>
</div>
<div class="content">
    <div class="main">
        <div class="about-img">
            <?php if($main_image == ''):?>
                <div style="background-image: url(/static/img/about.jpg)" class="img"></div>
            <?php else:?>
                <div style="background-image: url(<?php echo $main_image;?>)" class="img"></div>
            <?php endif?>
            <div class="img-title visible-xs">
                <?php if($title == ''):?>
                    <h1>PATRICIO RODRIGUEZ</h1>
                <?php else:?>
                    <h1><?php echo $title?></h1>
                <?php endif?>

                <?php if($sub_title == ''):?>
                    <h2>ABOUT</h2>
                <?php else:?>
                    <h2><?php echo $sub_title?></h2>
                <?php endif?>
            </div>
        </div>
        <div class="about-text">
            <div class="main-title hidden-xs">
                <?php if($title == ''):?>
                    <h1>PATRICIO RODRIGUEZ</h1>
                <?php else:?>
                    <h1><?php echo $title?></h1>
                <?php endif?>
                <?php if($sub_title == ''):?>
                    <h2>ABOUT</h2>
                <?php else:?>
                    <h2><?php echo $sub_title?></h2>
                <?php endif?>
            </div>
            <div class="main-text">
                <?php if($long_description != ''):?>
                    <?php echo $long_description?>
                <?php else:?>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown.</p>
                <?php endif?>
            </div>
            <div class="about-buttons">
                <a href="<?php echo $tab1_document?>" target="_blank" class="btn btn-default">
                    <?php if($tab1_label == ''):?>
                        VIEW RESUME
                    <?php else:?>
                        <?php echo $tab1_label?>
                    <?php endif?>
                </a>
                <a href="<?php if($tab2_link == ""):?><?php echo $this->createUrl('/exhibitions')?><?php else: echo $tab2_link; endif?>" class="btn btn-default">
                    <?php if($tab2_label == ''):?>
                        EXHIBITIONS
                    <?php else:?>
                        <?php echo $tab2_label?>
                    <?php endif?>
                </a>
                <a href="<?php if($tab3_link == ""):?><?php echo $this->createUrl('/artwork')?><?php else: echo $tab3_link; endif?>" class="btn btn-default">
                    <?php if($tab3_label == ''):?>
                        ARTWORK
                    <?php else:?>
                        <?php echo $tab3_label?>
                    <?php endif?>
                </a>
            </div>
        </div>
    </div>
</div>