<div class="left-bar"><a href="<?php echo $this->createUrl("/contact")?>" class="btn btn-default"><i class="fa fa-angle-up fa-3x"></i></a><a href="<?php echo $this->createUrl("/about")?>" class="btn btn-default"><i class="fa fa-angle-down fa-3x"></i></a></div>
<div class="right-bar"><i class="fa fa-circle active"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i>
</div>
<div class="content">
    <div class="main">
        <div id="home-carousel" data-ride="carousel" class="carousel slide">
            <div class="carousel-inner">
                <?php foreach($home_data as $key=>$hd):?>
                <div class="item <?php if($key==0):?>active<?php endif?>">
                    <a href="<?php echo $hd['banner_link']?>">
                    <div style="background-image: url(<?php echo $hd['image'] ?>)" class="img"></div>
                    </a>
                    <?php if($hd['sub_title'] != "" or $hd['title'] != "" ):?>
                        <div class="text">
                            <?php if($hd['title'] != "" ):?>
                                <h1><?php echo $hd['title']?></h1>
                            <?php endif?>

                            <?php if($hd['sub_title'] != "" ):?>
                                <h2><?php echo $hd['sub_title']?></h2>
                            <?php endif?>
                        </div>
                    <?php endif?>
                </div>
                <?php endforeach?>
<!--                <div class="item">-->
<!--                    <div style="background-image: url(/static/img/home-1.jpg)" class="img"></div>-->
<!--                </div>-->
<!--                <div class="item">-->
<!--                    <div style="background-image: url(/static/img/home-1.jpg)" class="img"></div>-->
<!--                </div>-->
            </div>
        </div>
    </div>
</div>