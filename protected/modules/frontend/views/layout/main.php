
<html>
  <head>
    <?php $this->widget('application.modules.irseo.widgets.IRSeo'); ?>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <?php
          Yii::app()->clientScript->scriptMap = array(
          'bootstrap.min.css' => false,
          'font-awesome.min.css' => false,
          'bootstrap-yii.css' => false,
          'jquery-ui-bootstrap.css' => false,

          'jquery.min.js'=>false,
          'jquery.js'=>false,
          'bootstrap-noconflict.js' => false,
          'bootbox.min.js' => false,
          'notify.min.js' => false,
          'bootstrap.min.js' => false,
          );  // OJO

          $base_url = Yii::app()->request->baseUrl;
          $cs = Yii::app()->clientScript;
          $action_id = Yii::app()->controller->action->id;
          ?>


      <!--[if lte IE 9]>
    <script src="/static/js/html5shiv.js"></script><![endif]-->
    <!--[if lte IE 8]>
    <script src="/static/js/respond.min.js"></script><![endif]-->
    <link href="/static/css/font-awesome.css" rel="stylesheet"/>
    <link href="/static/css/bootstrap_frontend.css" rel="stylesheet"/>
    <!--link(href="/static/css/jquery-ui.min.css",rel="stylesheet")-->
    <link href="/static/css/style.css" rel="stylesheet"/><!--[if IE]>
    <!--link(href="/static/css/ie.css",rel="stylesheet")--><![endif]-->
  </head>
  <body>
    <div class="navbar"><a href="/"><img src="<?php echo $this->common_data['site_logo']?>" class="logo"/></a>
      <button type="button" data-toggle="collapse" data-target="#navbar-menu" class="btn btn-default"><i class="fa fa-bars fa-2x"></i></button>
    </div>
    <div id="navbar-menu" class="collapse">
      <ul>
        <li class="active"><a href="/"><?php echo $this->common_data['main_menu']->home?></a></li>
        <li><a href="<?php echo $this->createUrl('/about')?>"><?php echo $this->common_data['main_menu']->about_us?></a></li>
        <li><a href="<?php echo $this->createUrl('/exhibitions')?>"><?php echo $this->common_data['main_menu']->exhibitions?></a></li>
        <li><a href="<?php echo $this->createUrl('/artwork')?>"><?php echo $this->common_data['main_menu']->artwork?></a></li>
        <li><a href="<?php echo $this->createUrl('/contact')?>"><?php echo $this->common_data['main_menu']->contact_us?></a></li>
      </ul>
    </div>
    <?php echo $content?>
    <div class="footer">
      <div class="social"><?php if($this->common_data['social_links']->facebook != ''):?><a href="<?php echo $this->common_data['social_links']->facebook?>"><i class="fa fa-facebook"></i></a><?php endif;?><?php if($this->common_data['social_links']->twitter != ''):?><a href="<?php echo $this->common_data['social_links']->twitter?>"><i class="fa fa-twitter"></i></a><?php endif;?><?php if($this->common_data['social_links']->google != ''):?><a href="<?php echo $this->common_data['social_links']->google?>"><i class="fa fa-google-plus"></i></a><?php endif;?><?php if($this->common_data['social_links']->youtube != ''):?><a href="<?php echo $this->common_data['social_links']->youtube?>"><i class="fa fa-youtube"></i></a><?php endif;?><?php if($this->common_data['social_links']->linkedin != ''):?><a href="<?php echo $this->common_data['social_links']->linkedin?>"><i class="fa fa-linkedin"></i></a><?php endif;?><?php if($this->common_data['social_links']->instagram != ''):?><a href="<?php echo $this->common_data['social_links']->instagram?>"><i class="fa fa-instagram"></i></a><?php endif;?></div>
      <div class="footer-menu">
        <ul>
          <li class="btn-group dropup"><a href="#" data-toggle="dropdown" class="btn dropdown-toggle">CATALOG</a>
            <ul class="dropdown-menu">
              <li>
                <h2><?php echo $this->common_data['footer_menu']->catalog_label?></h2>
              </li>
               <?php foreach($this->common_data['catalog'] as $catalog):?>
                   <?php if(file_exists(Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR .$catalog->_file->relativeWebRootFolder .'/file_'. $catalog->id . '.pdf')):?>
                       <li><a href="<?php echo '/uploads/file_'.$catalog->id.'.pdf'?>"><?php echo $catalog->title_file?></a></li>
                   <?php endif;?>
                <?php endforeach;?>
            </ul>
          </li>
          <li class="btn-group dropup"><a href="#" data-toggle="dropdown" class="btn dropdown-toggle">USEFUL LINKS</a>
            <ul class="dropdown-menu">
              <li>
                <h2><?php echo $this->common_data['footer_menu']->useful_link_labels?></h2>
              </li>
              <?php foreach($this->common_data['usefullinks'] as $usefullink):?>
                    <li><a href="<?php echo $usefullink->link_url?>"><?php echo $usefullink->link_text?></a></li>
              <?php endforeach;?>
            </ul>
          </li>
            <?php if(file_exists(Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR .$this->common_data['resume']->_file->relativeWebRootFolder .'/file_'. $this->common_data['resume']->id . '.pdf')):?>
                <li><a href="/uploads/file_<?php echo $this->common_data['resume']->id?>.pdf" class="btn"><?php echo strtoupper($this->common_data['footer_menu']->resume_label)?></a></li>
            <?php endif;?>
        </ul>
      </div>
      <div class="copy">
        <div class="hidden-xs">Copyright © <?php echo $this->common_data['copy_right']->copyright_text?> Gallery <?php echo date('Y')?>. All Rights Reserved.</div>
        <div class="visible-xs">Copyright © <?php echo $this->common_data['copy_right']->copyright_text?> <br> Gallery <?php echo date('Y')?>. All Rights Reserved.</div>
      </div>
      <div class="design">WEBSITE DESIGN & DEVELOPMENT BY <a href="#">THRUADS.COM</a> CORP.</div>
    </div>
    <script src="/static/js/jquery-1.11.1.min.js"></script>
    <script src="/static/js/bootstrap.min.js"></script>
    <!--script(src="/static/js/jquery-ui.min.js")-->
    <script src="/static/js/custom.js"></script>
  </body>
</html>