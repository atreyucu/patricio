<div class="left-bar"><a href="<?php echo $this->createUrl("/artwork")?>" class="btn btn-default"><i class="fa fa-angle-up fa-3x"></i></a>
    <a href="/" class="btn btn-default"><i class="fa fa-angle-down fa-3x"></i></a></div>
<div class="right-bar"><i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle active"></i>
</div>
<div class="content">
    <div class="main">
        <div class="about-img contact-img">
            <?php if($image == ""):?>
                <div style="background-image: url(/static/img/contact.jpg)" class="img contact"></div>
            <?php else:?>
                <div style="background-image: url(<?php echo $image?>)" class="img contact"></div>
            <?php endif?>
            <div class="img-title visible-xs">
                <?php if($title == ""):?>
                    <h1>PATRICIO RODRIGUEZ</h1>
                <?php else:?>
                    <h1><?php echo $title?></h1>
                <?php endif?>
                <?php if($sub_title == ""):?>
                    <h2>CONTACT</h2>
                <?php else:?>
                    <h2><?php echo $sub_title?></h2>
                <?php endif?>
            </div>
        </div>
        <div class="about-text contact-text">
            <div class="main-title hidden-xs">
                <?php if($title == ""):?>
                    <h1>PATRICIO RODRIGUEZ</h1>
                <?php else:?>
                    <h1><?php echo $title?></h1>
                <?php endif?>
                <?php if($sub_title == ""):?>
                    <h2>CONTACT</h2>
                <?php else:?>
                    <h2><?php echo $sub_title?></h2>
                <?php endif?>
            </div>
            <div class="contact-details">
                <p class="links"><?php if($email_title == ""):?>E-mail<?php else: echo $email_title; endif?>:
                    <a href="#">
                        <?php if($email == ""):?>
                            patricioart@gmail.com
                        <?php else:?>
                            <?php echo $email;?>
                        <?php endif?>
                    </a> |

                    <?php if($phone_title == ""):?>Phone:<?php else: echo $phone_title; endif?>:
                    <a href="#"><?php if($phone == ""):?>305-000-000<?php else: echo $phone; endif?></a></p>
                <p>
                    <?php echo $description?>
<!--                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.-->
                </p>
            </div>
            <form class="contact-form" action="/frontend/contact/sendEmail" method="post">
                <div class="form-group">
                    <input type="text" name="name" placeholder="Your Name (requiered)" required class="form-control"/>
                </div>
                <div class="form-group">
                    <input type="email" name="email" placeholder="E-Mail (requiered)" required class="form-control"/>
                </div>
                <div class="form-group">
                    <input type="text" name="subject" placeholder="Subject" required class="form-control"/>
                </div>
                <div class="form-group message">
                    <textarea name="message" placeholder="Message" rows="5" required class="form-control"></textarea>
                </div>
                <button type="submit" class="btn btn-default">SEND</button>
            </form>
        </div>
    </div>
</div>