<?php

class ThemeModule extends CWebModule
{
    public function getName(){
        return 'Theme';
    }

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

        $this->attachBehavior('ycm', array(
            'class' => 'ycm.behaviors.YModuleBehavior'
        ));

		// import the module-level models and components
		$this->setImport(array(
			'theme.models.*',
			'theme.components.*',
		));
	}
    public function getAdminMenus(){
        parent::getAdminMenus();
        $menu = array(
            array(
                'url' => '#',
                'icon' => 'indent-left',
                'label' => t('Header Information'),
                'visible' => user()->havePermission(array('Level-3')),
                'items' => array(
                    array(
                        'icon' => 'picture',
                        'label' => Yii::t('sideMenu',t('Site Logo')),
                        'url' => array('/theme/generalHeaderInformation/admin'),
                        'active' => Yii::app()->controller->module->id == 'theme' && Yii::app()->controller->id == 'generalHeaderInformation',
                        'visible' => user()->havePermission(array('Level-3')),
                    ),
                    array(
                        'icon' => 'list-alt',
                        'label' => Yii::t('sideMenu',t('Navigation Menu')),
                        'url' => array('/theme/mainNavegationMenu/admin'),
                        'active' => Yii::app()->controller->module->id == 'theme' && Yii::app()->controller->id == 'mainNavegationMenu',
                        'visible' => user()->havePermission(array('Level-3')),
                    ),
                )),
            array(
                'url' => '#',
                'icon' => 'home',
                'label' => t('Home Information'),
                'visible' => user()->havePermission(array('Level-3')),
                'items' => array(
                    array(
                        'icon' => 'tasks',
                        'label' => Yii::t('sideMenu',t('Home Page')),
                        'url' => array('/theme/homePage/admin'),
                        'active' => Yii::app()->controller->module->id == 'theme' && Yii::app()->controller->id == 'homePage',
                        'visible' => user()->havePermission(array('Level-3')),
                    ),
                )),
            array(
                'url' => '#',
                'icon' => 'folder-close',
                'label' => t('About us'),
                'visible' => user()->havePermission(array('Level-3')),
                'items' => array(
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu',t('About Page')),
                        'url' => array('/theme/aboutContent/admin'),
                        'active' => Yii::app()->controller->module->id == 'theme' && Yii::app()->controller->id == 'aboutContent',
                        'visible' => user()->havePermission(array('Level-3')),
                    ),
                )),
            array(
            'url' => '#',
            'icon' => 'briefcase',
            'label' => t('Exhibitions'),
            'visible' => user()->havePermission(array('Level-3')),
            'items' => array(
                        array(
                            'icon' => 'file',
                            'label' => Yii::t('sideMenu',t('Exhibitions Section 1')),
                            'url' => array('/theme/exhibitionSection1/admin'),
                            'active' => Yii::app()->controller->module->id == 'theme' && Yii::app()->controller->id == 'exhibitionSection1',
                            'visible' => user()->havePermission(array('Level-3')),
                        ),
                        array(
                            'icon' => 'file',
                            'label' => Yii::t('sideMenu',t('Exhibitions Section 2')),
                            'url' => array('/theme/exhibitionSection2/admin'),
                            'active' => Yii::app()->controller->module->id == 'theme' && Yii::app()->controller->id == 'exhibitionSection2',
                            'visible' => user()->havePermission(array('Level-3')),
                        ),

            )),
            array(
                'url' => '#',
                'icon' => 'envelope',
                'label' => t('Contact Us'),
                'visible' => user()->havePermission(array('Level-3')),
                'items' => array(
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu',t('Contact Page')),
                        'url' => array('/theme/contactUs/admin'),
                        'active' => Yii::app()->controller->module->id == 'theme' && Yii::app()->controller->id == 'contactUs',
                        'visible' => user()->havePermission(array('Level-3')),
                    ),
                )),
            array(
                'url' => '#',
                'icon' => 'tag',
                'label' => t('Artwork'),
                'visible' => user()->havePermission(array('Level-3')),
                'items' => array(
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu',t('Artwork Main Section')),
                        'url' => array('/theme/artworkMain/admin'),
                        'active' => Yii::app()->controller->module->id == 'theme' && Yii::app()->controller->id == 'artworkMain',
                        'visible' => user()->havePermission(array('Level-3')),
                    ),
                    array(
                        'icon' => 'filter',
                        'label' => Yii::t('sideMenu',t('Artworks Top Filters')),
                        'url' => array('/theme/artworkTopFilter/admin'),
                        'active' => Yii::app()->controller->module->id == 'theme' && Yii::app()->controller->id == 'artworkTopFilter',
                        'visible' => user()->havePermission(array('Level-3')),
                    ),
                    array(
                        'icon' => 'filter',
                        'label' => Yii::t('sideMenu',t('Artworks Bottom Filters')),
                        'url' => array('/theme/artworkBottomFilter/admin'),
                        'active' => Yii::app()->controller->module->id == 'theme' && Yii::app()->controller->id == 'artworkBottomFilter',
                        'visible' => user()->havePermission(array('Level-3')),
                    ),
                    array(
                        'icon' => 'tags',
                        'label' => Yii::t('sideMenu',t('Artworks Gallery')),
                        'url' => array('/theme/artworkGallery/admin'),
                        'active' => Yii::app()->controller->module->id == 'theme' && Yii::app()->controller->id == 'artworkGallery',
                        'visible' => user()->havePermission(array('Level-3')),
                    ),
                )),
            array(
                'url' => '#',
                'icon' => 'leaf',
                'label' => 'Footer',
                'visible' => true,
                'items' => array(
                    array(
                        'icon' => 'star-empty',
                        'label' => Yii::t('sideMenu',t('Footer Menu Options')),
                        'url' => array('/theme/footerMenu/admin'),
                        'active' => Yii::app()->controller->module->id == 'theme' && Yii::app()->controller->id == 'footerMenu',
                        'visible' => user()->havePermission(array('Level-3')),
                    ),
                    array(
                        'icon' => 'cloud-download',
                        'label' => Yii::t('sideMenu',t('Catalogs')),
                        'url' => array('/theme/footerCatalog/admin'),
                        'active' => Yii::app()->controller->module->id === 'theme' && Yii::app()->controller->id === 'footerCatalog',
                        'visible' => user()->havePermission(array('Level-3')),
                    ),
                    array(
                        'icon' => 'link',
                        'label' => Yii::t('sideMenu',t('Useful Links')),
                        'url' => array('/theme/footerUsefulLinks/admin'),
                        'active' => Yii::app()->controller->module->id === 'theme' && Yii::app()->controller->id === 'footerUsefulLinks',
                        'visible' => user()->havePermission(array('Level-3')),
                    ),
                    array(
                        'icon' => 'comment',
                        'label' => Yii::t('sideMenu','Social Media'),
                        'url' => array('/theme/socialNavegationMenu/admin'),
                        'active' => Yii::app()->controller->module->id === 'theme' && Yii::app()->controller->id === 'socialNavegationMenu',
                        'visible' => user()->havePermission(array('Level-3')),
                    ),
                    array(
                        'icon' => 'comment',
                        'label' => Yii::t('sideMenu','Resume File'),
                        'url' => array('/theme/footerResume/admin'),
                        'active' => Yii::app()->controller->module->id === 'theme' && Yii::app()->controller->id === 'footerResume',
                        'visible' => user()->havePermission(array('Level-3')),
                    ),
                    array(
                        'icon' => 'comment',
                        'label' => Yii::t('sideMenu','Copy Right Information'),
                        'url' => array('/theme/footerCopyright/admin'),
                        'active' => Yii::app()->controller->module->id === 'theme' && Yii::app()->controller->id === 'footerCopyright',
                        'visible' => user()->havePermission(array('Level-3')),
                    ),
                ),
            ),

        );
        return $menu;
    }


	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
