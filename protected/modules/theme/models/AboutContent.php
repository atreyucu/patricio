
<?php

Yii::import('theme.models._base.BaseAboutContent');

class AboutContent extends BaseAboutContent
{
    /**
    * @param string $className
    * @return AboutContent    */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function choices(){
        return GxHtml::listDataEx(self::model()->findAllAttributes(null, true));
    }

    public function elementos($attr_id, $attr){
        $v = $this->$attr_id;
        unset($this->$attr_id);
        $c = $this->search()->criteria;
        //$c->select = $attr_id;
        $all = self::model()->findAll($c);
        $this->$attr_id = $v;

        $r = array();
        foreach($all as $p){
            $r[$p->$attr_id] = $p->$attr;
        }
        return $r;
    }


    /**
    * Es llamado para saber si se muestra o no la administracion de este modelo
    * usar en conjunto con los permisos
    * @return boolean
    */
    public function hasAdmin(){
        return false;
    }


    public function init(){
        $this->id = rand_uniqid();
    }




    /**
    * Admin variables (ycm module)
    */
    public $adminNames=array('AboutContents','aboutcontent','aboutcontents','About'); // admin interface, singular, plural
    public $downloadExcel=false; // Download Excel
    public $downloadMsCsv=false; // Download MS CSV
    public $downloadCsv=false; // Download CSV


    /**
    * Config for attribute widgets (ycm module)
    *
    * @return array
    */
    public function attributeWidgets()
    {
        return array(
                array('id', 'textField'),
                    array('main_image', 'textField'),
                    array('title', 'textField'),
                    array('sub_title', 'textField'),
                    array('long_description', 'textField'),
                    array('tab1_label', 'textField'),
                    array('tab2_label', 'textField'),
                    array('tab2_link', 'textField'),
                    array('tab3_label', 'textField'),
                    array('tab3_link', 'textField'),
            );
    }


    /**
    * Config for TbGridView class (ycm module)
    *
    * @return array
    */
    public function adminSearch()
    {
        return array(
            'columns'=>array(
				'main_image',
				'title',
				'sub_title',
				'long_description',
				'tab1_label',
				'tab2_label',
				/*
				'tab2_link',
				'tab3_label',
				'tab3_link',
			*/
            ),
        );
    }


    /**
    * Config for TbDetailView class (ycm module)
    *
    * @return array
    */
    public function details()
    {
        return array(
            'attributes'=>array(
                				'main_image',
				'title',
				'sub_title',
				'long_description',
				'tab1_label',
				'tab2_label',
				/*
				'tab2_link',
				'tab3_label',
				'tab3_link',
			*/
            ),
        );
}



}