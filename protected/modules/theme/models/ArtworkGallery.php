
<?php

Yii::import('theme.models._base.BaseArtworkGallery');

class ArtworkGallery extends BaseArtworkGallery
{
    /**
    * @param string $className
    * @return ArtworkGallery    */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function choices(){
        return GxHtml::listDataEx(self::model()->findAllAttributes(null, true));
    }

    public function elementos($attr_id, $attr){
        $v = $this->$attr_id;
        unset($this->$attr_id);
        $c = $this->search()->criteria;
        //$c->select = $attr_id;
        $all = self::model()->findAll($c);
        $this->$attr_id = $v;

        $r = array();
        foreach($all as $p){
            $r[$p->$attr_id] = $p->$attr;
        }
        return $r;
    }


    /**
    * Es llamado para saber si se muestra o no la administracion de este modelo
    * usar en conjunto con los permisos
    * @return boolean
    */
    public function hasAdmin(){
        return false;
    }


    public function init(){
        $this->id = rand_uniqid();
    }




    /**
    * Admin variables (ycm module)
    */
    public $adminNames=array('ArtworkGalleries','artworkgallery','artworkgalleries','Artwork'); // admin interface, singular, plural
    public $downloadExcel=false; // Download Excel
    public $downloadMsCsv=false; // Download MS CSV
    public $downloadCsv=false; // Download CSV


    /**
    * Config for attribute widgets (ycm module)
    *
    * @return array
    */
    public function attributeWidgets()
    {
        return array(
                array('id', 'textField'),
                    array('listing_image', 'textField'),
                    array('zoom_image', 'textField'),
                    array('show_order', 'spinner'),
                    array('title', 'textField'),
                    array('short_description', 'textField'),
                    array('short_description2', 'textField'),
                    array('dimension_text', 'textField'),
                    array('author_name', 'textField'),
                    array('long_description', 'wysiwyg'),
                    array('popup_button_label', 'textField'),
                    array('popup_button_link', 'textField'),
                    array('country', 'spinner'),
                    array('art_technique', 'spinner'),
            );
    }


    /**
    * Config for TbGridView class (ycm module)
    *
    * @return array
    */
    public function adminSearch()
    {
        return array(
            'columns'=>array(
				'listing_image',
				'zoom_image',
				'show_order',
				'title',
				'short_description',
				'short_description2',
				/*
				'dimension_text',
				'author_name',
				'long_description',
				'popup_button_label',
				'popup_button_link',
				'country',
				'art_technique',
			*/
            ),
        );
    }


    /**
    * Config for TbDetailView class (ycm module)
    *
    * @return array
    */
    public function details()
    {
        return array(
            'attributes'=>array(
                				'listing_image',
				'zoom_image',
				'show_order',
				'title',
				'short_description',
				'short_description2',
				/*
				'dimension_text',
				'author_name',
				'long_description',
				'popup_button_label',
				'popup_button_link',
				'country',
				'art_technique',
			*/
            ),
        );
}



}