
<?php

Yii::import('theme.models._base.BaseExhibitionSection1');

class ExhibitionSection1 extends BaseExhibitionSection1
{
    /**
    * @param string $className
    * @return ExhibitionSection1    */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function choices(){
        return GxHtml::listDataEx(self::model()->findAllAttributes(null, true));
    }

    public function elementos($attr_id, $attr){
        $v = $this->$attr_id;
        unset($this->$attr_id);
        $c = $this->search()->criteria;
        //$c->select = $attr_id;
        $all = self::model()->findAll($c);
        $this->$attr_id = $v;

        $r = array();
        foreach($all as $p){
            $r[$p->$attr_id] = $p->$attr;
        }
        return $r;
    }


    /**
    * Es llamado para saber si se muestra o no la administracion de este modelo
    * usar en conjunto con los permisos
    * @return boolean
    */
    public function hasAdmin(){
        return false;
    }


    public function init(){
        $this->id = rand_uniqid();
    }




    /**
    * Admin variables (ycm module)
    */
    public $adminNames=array('ExhibitionSection1s','exhibitionsection1','exhibitionsection1s','Exhibitions'); // admin interface, singular, plural
    public $downloadExcel=false; // Download Excel
    public $downloadMsCsv=false; // Download MS CSV
    public $downloadCsv=false; // Download CSV


    /**
    * Config for attribute widgets (ycm module)
    *
    * @return array
    */
    public function attributeWidgets()
    {
        return array(
                array('id', 'textField'),
                    array('filter_label', 'textField'),
                    array('section1_title', 'textField'),
                    array('section1_subtitle', 'textField'),
                    array('section1_long_description', 'wysiwyg'),
            );
    }


    /**
    * Config for TbGridView class (ycm module)
    *
    * @return array
    */
    public function adminSearch()
    {
        return array(
            'columns'=>array(
				'filter_label',
				'section1_title',
				'section1_subtitle',
				'section1_long_description',
            ),
        );
    }


    /**
    * Config for TbDetailView class (ycm module)
    *
    * @return array
    */
    public function details()
    {
        return array(
            'attributes'=>array(
                				'filter_label',
				'section1_title',
				'section1_subtitle',
				'section1_long_description',
            ),
        );
}



}