<?php

/**
 * This is the model base class for the table "footer_copyright".
 * It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "FooterCopyright".
 * This code was improve iReevo Team
 * Columns in table "footer_copyright" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $id
 * @property string $copyright_text
 *
 * @property Date2TimeBehavior $date2time
 * @property CurrencyBehavior $currency
 * @property ImageARBehavior $imageAR

 */
abstract class BaseFooterCopyright extends GxActiveRecord {

/* si tiene una imagen pa subir con ImageARBehavior, descomente la linea siguiente
// public $recipeImg;

    /**
    * Behaviors.
    * @return array
    */
    function behaviors() {
        return CMap::mergeArray(parent::behaviors(), array(
                

                                'files' => array(
                     'class'=>'application.modules.ycm.behaviors.FileBehavior',
                ),
                'date2time' => array(
                    'class' => 'ycm.behaviors.Date2TimeBehavior',
                    'attributes'=>'',
                    'format'=>'Y-m-d',
                ),
                'datetime2time' => array(
                    'class' => 'ycm.behaviors.Date2TimeBehavior',
                    'attributes'=>'',
                    'format'=>'Y-m-d H:i:s',
                ),
                'currency' => array(
                    'class' => 'ycm.behaviors.CurrencyBehavior',
                    'attributes'=>'',
                ),
                            ));
    }


	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'footer_copyright';
	}

	public static function label($n = 1) {
		return self::model()->t_model('FooterCopyright|FooterCopyrights', $n);
	}

	public static function representingColumn() {
		return 'copyright_text';
	}

	public function rules() {
		return array(
			array('id, copyright_text', 'required'),
			array('id', 'length', 'max'=>50),
			array('copyright_text', 'length', 'max'=>70),

/* descomente las lineas siguientes si quiere subir una image con ImageARBehavior*/


			array('id, copyright_text', 'safe', 'on'=>'search'),
        		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => $this->t_label('ID'),
			'copyright_text' => $this->t_label('Copy Right Company Name'),

		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('copyright_text', $this->copyright_text, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            		));
	}
}