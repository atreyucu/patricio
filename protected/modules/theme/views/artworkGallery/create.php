

<?php





$this->breadcrumbs = array(
    $model->adminNames[3] ,'Artwork Gallery' => array('admin'),
	t('Add'),
);

$this->title = Yii::t('YcmModule.ycm',
    'Create Artwork Gallery',
    array('{name}'=>$model->adminNames[3])
);

$this->renderPartial('_form', array(
    'model' => $model,
    'buttons' => 'create'));

