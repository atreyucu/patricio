
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>
<?php $form = $this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

        <?php echo $form->textFieldGroup($model, 'id', array(
                'maxlength' => 50,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => t('Please, insert id'),
                                'append' => 'Text'
                                )
                      ); ?>
        <?php echo $form->fileFieldGroup($model, 'recipeImg1',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            'append' => CHtml::image($model->_listing_image->getFileUrl('normal'), '', array('width' => '100px')),
                            'hint' => t('The image dimensions are 561x274px')
                        ));
                       echo $form->textFieldGroup($model, 'listing_image',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => t('Please, insert listing_image'),
                                'append' => 'Text',
                            )
                        ); ?>
        <?php echo $form->fileFieldGroup($model, 'recipeImg2',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            'append' => CHtml::image($model->_zoom_image->getFileUrl('normal'), '', array('width' => '100px')),
                            'hint' => t('The image dimensions are 561x274px')
                        ));
                       echo $form->textFieldGroup($model, 'zoom_image',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => t('Please, insert zoom_image'),
                                'append' => 'Text',
                            )
                        ); ?>
        <?php echo $form->dropDownListGroup($model,'show_order',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => array(t('High'),t('Medium'),t('Low')),
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                                // 'hint' => t('Please, select show_order'),
                                 'prepend' => 'Select'
                            )
                        ); ?>
        <?php echo $form->textFieldGroup($model, 'title', array(
                'maxlength' => 20,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => t('Please, insert title'),
                                'append' => 'Text'
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'short_description', array(
                'maxlength' => 25,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => t('Please, insert short_description'),
                                'append' => 'Text'
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'short_description2', array(
                'maxlength' => 25,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => t('Please, insert short_description2'),
                                'append' => 'Text'
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'dimension_text', array(
                'maxlength' => 40,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => t('Please, insert dimension_text'),
                                'append' => 'Text'
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'author_name', array(
                'maxlength' => 70,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => t('Please, insert author_name'),
                                'append' => 'Text'
                                )
                      ); ?>
        <?php echo $form->redactorGroup($model,'long_description',
                array(
                    'widgetOptions' => array(
                            'editorOptions' =>array(
                                'class' => 'span4',
                                'rows' => 10,
                                'options' => array('plugins' => array('clips', 'fontfamily','fullscreen'), 'lang'=>app()->getLanguage())
                            )
                        )
                    )
                ); ?>
        <?php echo $form->textFieldGroup($model, 'popup_button_label', array(
                'maxlength' => 20,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => t('Please, insert popup_button_label'),
                                'append' => 'Text'
                                )
                      ); ?>
        <?php echo $form->urlFieldGroup($model,'popup_button_link',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            //'hint' => t('Please, insert popup_button_link'),
                            'append' => 'http://'
                        )
                    ); ?>
        <?php echo $form->dropDownListGroup($model,'country',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => array(t('MEXICO'),t('UNITED STATES'),t('CUBA')),
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                                // 'hint' => t('Please, select country'),
                                 'prepend' => 'Select'
                            )
                        ); ?>
        <?php echo $form->dropDownListGroup($model,'art_technique',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => array(t('HAND PAPER CUT'),t('WOOD DRAFT'),t('MIX TECHNIQUE')),
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                                // 'hint' => t('Please, select art_technique'),
                                 'prepend' => 'Select'
                            )
                        ); ?>

<div class="form-actions">
    		<?php $this->widget('application.extensions.bootstrap.widgets.TbButton',
    array(
            'buttonType' => 'submit',
            'context' => 'success',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => t('Buscar '.mod('ycm')->getPluralName($model))
        ));
 ?></div>

<?php $this->endWidget(); ?>
