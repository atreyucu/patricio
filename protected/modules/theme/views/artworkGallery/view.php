
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php
$this->breadcrumbs = array(
        $model->adminNames[3] ,'Artwork Gallery'  => array('admin'),
    t('View'),
);


$this->title = Yii::t('YcmModule.ycm',
    'View Artwork Gallery',
    array('{name}'=>$model->adminNames[3])
);

?>

<?php $this->widget('application.extensions.bootstrap.widgets.TbEditableDetailView', array(
	'data' => $model,
    'id'=>'view-table',
	'attributes' => array(

                    array(
                            'label' => t('Artwork Listing Image'),
                            'type' => 'raw',
                            'value' => CHtml::image($model->_listing_image->getFileUrl('normal'), '', array('width'=> '100px')),
                        )
                ,

                    array(
                            'label' => t('Artwork Zoom Image'),
                            'type' => 'raw',
                            'value' => CHtml::image($model->_zoom_image->getFileUrl('normal'), '', array('width'=> '100px')),
                        )
                ,
array(
                    'class' => 'application.extensions.bootstrap.widgets.TbEditableColumn',
                    'name' => 'show_order',
                    'editable' => array(
                        'type'=>'select',
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'url' => array('/ycm/model/updateAttribute', 'model' => get_class($model)),
                        'source' => array(t('High'),t('Medium'),t('Low')),
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                            )
					),
array(
				    'class' => 'application.extensions.bootstrap.widgets.TbEditableColumn',
					'name' => 'title',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'url' => array('/ycm/model/updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
				    'class' => 'application.extensions.bootstrap.widgets.TbEditableColumn',
					'name' => 'short_description',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'url' => array('/ycm/model/updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
				    'class' => 'application.extensions.bootstrap.widgets.TbEditableColumn',
					'name' => 'short_description2',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'url' => array('/ycm/model/updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
				    'class' => 'application.extensions.bootstrap.widgets.TbEditableColumn',
					'name' => 'dimension_text',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'url' => array('/ycm/model/updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
				    'class' => 'application.extensions.bootstrap.widgets.TbEditableColumn',
					'name' => 'author_name',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'url' => array('/ycm/model/updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
				    'class' => 'application.extensions.bootstrap.widgets.TbEditableColumn',
					'name' => 'long_description',
					'sortable' => false,
					'editable' => array(
                            'type'=>'textarea',
                            'url' => array('/ycm/model/updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                             'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
				    'class' => 'application.extensions.bootstrap.widgets.TbEditableColumn',
					'name' => 'popup_button_label',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'url' => array('/ycm/model/updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
				    'class' => 'application.extensions.bootstrap.widgets.TbEditableColumn',
					'name' => 'popup_button_link',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'url' => array('/ycm/model/updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
                    'class' => 'application.extensions.bootstrap.widgets.TbEditableColumn',
                    'name' => 'country',
                    'editable' => array(
                        'type'=>'select',
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'url' => array('/ycm/model/updateAttribute', 'model' => get_class($model)),
                        'source' => array(t('MEXICO'),t('UNITED STATES'),t('CUBA')),
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                            )
					),
array(
                    'class' => 'application.extensions.bootstrap.widgets.TbEditableColumn',
                    'name' => 'art_technique',
                    'editable' => array(
                        'type'=>'select',
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'url' => array('/ycm/model/updateAttribute', 'model' => get_class($model)),
                        'source' => array(t('HAND PAPER CUT'),t('WOOD DRAFT'),t('MIX TECHNIQUE')),
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                            )
					),
	),
)); ?>

<div class="form-actions" style="text-align: right">
    <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo t('Back');?></a><?php // echo CHtml::link(t(TbHtml::icon('glyphicon glyphicon-briefcase'). 'Manage item'),array('admin'),array('class'=>'btn btn-default'));?><?php echo CHtml::link(t(TbHtml::icon('glyphicon glyphicon-saved'). 'Update item'),array('update','id'=>$model->id),array('class'=>'btn btn-primary'));?><?php if(user()->isAdmin):?>
<?php echo CHtml::link(t(TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger delete'));?><?php endif?></div>

<div id="success"  class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p><?php echo t('Data was saved with success');?></p>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script type="application/javascript">

    jQuery(document).on('click','a.delete',function() {
    if(!confirm('Are you sure you want to delete this item?')) return false;
        return true;

    });
</script>