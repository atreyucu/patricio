

<?php





$this->breadcrumbs = array(
    $model->adminNames[3] ,'Copy Right Information' => array('admin'),
	t('Add'),
);

$this->title = Yii::t('YcmModule.ycm',
    'Create Copy Right Information',
    array('{name}'=>$model->adminNames[3])
);

$this->renderPartial('_form', array(
    'model' => $model,
    'buttons' => 'create'));

