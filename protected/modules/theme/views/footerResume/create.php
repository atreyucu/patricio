

<?php





$this->breadcrumbs = array(
    $model->adminNames[3] ,'Resume File' => array('admin'),
	t('Add'),
);

$this->title = Yii::t('YcmModule.ycm',
    'Create Resume File',
    array('{name}'=>$model->adminNames[3])
);

$this->renderPartial('_form', array(
    'model' => $model,
    'buttons' => 'create'));

