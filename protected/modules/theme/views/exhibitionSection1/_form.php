
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php $form = $this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
    'id' => 'exhibition-section1-form',
    'enableClientValidation'=>true,

));
?>
    <?php echo $form->textFieldGroup($model, 'filter_label', array(
                'maxlength' => 15,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => t('Please, insert filter_label'),
                                'append' => 'Text'
                                )
                      ); ?>
    <?php echo $form->textFieldGroup($model, 'section1_title', array(
                'maxlength' => 35,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => t('Please, insert section1_title'),
                                'append' => 'Text'
                                )
                      ); ?>
    <?php echo $form->textFieldGroup($model, 'section1_subtitle', array(
                'maxlength' => 40,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => t('Please, insert section1_subtitle'),
                                'append' => 'Text'
                                )
                      ); ?>
    <?php echo $form->redactorGroup($model,'section1_long_description',
                array(
                    'widgetOptions' => array(
                            'editorOptions' =>array(
                                'class' => 'span4',
                                'rows' => 10,
                                'options' => array('plugins' => array('clips', 'fontfamily','fullscreen'), 'lang'=>app()->getLanguage())
                            )
                        )
                    )
                ); ?>
<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo t('Back');?></a>   <?php $this->widget(
        'application.extensions.bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => t('Save item')
        )
    ); ?>
    <?php $this->widget(
        'application.extensions.bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'reset',
            'context' => 'warning',
            'icon'=> 'glyphicon glyphicon-remove',
            'label' => t('Reset form')
        )
    ); ?>
    <?php $this->endWidget(); ?>
    <?php if(isset($model->id)){
       // echo CHtml::link(t(TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?></div>