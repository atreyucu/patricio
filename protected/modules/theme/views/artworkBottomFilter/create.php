

<?php





$this->breadcrumbs = array(
    $model->adminNames[3] ,'Artwork Bottom Filters' => array('admin'),
	t('Add'),
);

$this->title = Yii::t('YcmModule.ycm',
    'Create Artwork Bottom Filters',
    array('{name}'=>$model->adminNames[3])
);

$this->renderPartial('_form', array(
    'model' => $model,
    'buttons' => 'create'));

