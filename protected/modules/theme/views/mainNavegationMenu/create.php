

<?php





$this->breadcrumbs = array(
    $model->adminNames[3] ,'Navigation Menu' => array('admin'),
	t('Add'),
);

$this->title = Yii::t('YcmModule.ycm',
    'Create Navigation Menu',
    array('{name}'=>$model->adminNames[3])
);

$this->renderPartial('_form', array(
    'model' => $model,
    'buttons' => 'create'));

