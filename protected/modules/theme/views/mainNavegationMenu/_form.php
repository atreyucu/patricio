
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php $form = $this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
    'id' => 'main-navegation-menu-form',
    'enableClientValidation'=>true,

));
?>
    <?php echo $form->textFieldGroup($model, 'about_us', array(
                'maxlength' => 15,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => t('Please, insert about_us'),
                                'append' => 'Text'
                                )
                      ); ?>
    <?php echo $form->textFieldGroup($model, 'exhibitions', array(
                'maxlength' => 15,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => t('Please, insert exhibitions'),
                                'append' => 'Text'
                                )
                      ); ?>
    <?php echo $form->textFieldGroup($model, 'artwork', array(
                'maxlength' => 15,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => t('Please, insert artwork'),
                                'append' => 'Text'
                                )
                      ); ?>
    <?php echo $form->textFieldGroup($model, 'contact_us', array(
                'maxlength' => 15,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => t('Please, insert contact_us'),
                                'append' => 'Text'
                                )
                      ); ?>
    <?php echo $form->textFieldGroup($model, 'home', array(
                'maxlength' => 15,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                               // 'hint' => t('Please, insert home'),
                                'append' => 'Text'
                                )
                      ); ?>
<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo t('Back');?></a>   <?php $this->widget(
        'application.extensions.bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => t('Save item')
        )
    ); ?>
    <?php $this->widget(
        'application.extensions.bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'reset',
            'context' => 'warning',
            'icon'=> 'glyphicon glyphicon-remove',
            'label' => t('Reset form')
        )
    ); ?>
    <?php $this->endWidget(); ?>
    <?php if(isset($model->id)){
       // echo CHtml::link(t(TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?></div>