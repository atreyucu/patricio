

<?php





$this->breadcrumbs = array(
    $model->adminNames[3] ,'Footer Menu Options' => array('admin'),
	t('Add'),
);

$this->title = Yii::t('YcmModule.ycm',
    'Create Footer Menu Options',
    array('{name}'=>$model->adminNames[3])
);

$this->renderPartial('_form', array(
    'model' => $model,
    'buttons' => 'create'));

