

<?php





$this->breadcrumbs = array(
    $model->adminNames[3] ,'Artwork Top Filters' => array('admin'),
	t('Add'),
);

$this->title = Yii::t('YcmModule.ycm',
    'Create Artwork Top Filters',
    array('{name}'=>$model->adminNames[3])
);

$this->renderPartial('_form', array(
    'model' => $model,
    'buttons' => 'create'));

