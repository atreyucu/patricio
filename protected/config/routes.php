<?php
return array(
    /*array(
        'class' => 'IRSeoUrlRule',
        'modelClass' => 'CategoriaProgol',
        'viewRoute' => 'categoriaProgol/view',
    ),*/
    'frontend' => 'frontend',
    'admin' => 'user/login/login',
    /*array(
        'class' => 'CUrlRule',
        'routePattern' => 'contacto',
        'route' => 'site/contact',
    ),*/
    //'contacto' => 'site/contact',
    //'<view:politicas>' => array('site/page'),

    'irseo'=>'irseo',
    'irseo/<controller:\w+>'=>'irseo/<controller>',
    'irseo/<controller:\w+>/<action:\w+>'=>'irseo/<controller>/<action>',

    'irtext'=>'irtext',
    'irtext/<controller:\w+>'=>'irtext/<controller>',
    'irtext/<controller:\w+>/<action:\w+>'=>'irtext/<controller>/<action>',

    'ycm'=>'ycm',
    'ycm/<controller:\w+>'=>'ycm/<controller>',
    'ycm/<controller:\w+>/<action:\w+>'=>'ycm/<controller>/<action>',
    'user'=>'user',
    'user/<controller:\w+>'=>'user/<controller>',
    'user/<controller:\w+>/<action:\w+>'=>'user/<controller>/<action>',
    'gii'=>'gii',
    'gii/<controller:\w+>'=>'gii/<controller>',
    'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',
    '<controller:\w+>/<id:\d+>'=>'<controller>/view',
    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',

    'cupon'=>'cupon',
    'cupon/<controller:\w+>'=>'cupon/<controller>',
    'cupon/<controller:\w+>/<action:\w+>'=>'cupon/<controller>/<action>',

    'frontend'=>'frontend',
    'frontend/<controller>'=>'frontend/<controller>',
    'frontend/<controller>/<action:\w+>'=>'frontend/<controller>/<action>',
    #'index'=>'frontend/default/index',
    #'contact-us'=>'frontend/default/contact_us',
    #'about-us'=>'frontend/default/about_us',
    #'all-services'=>'frontend/default/all_services',
    #'other'=>'frontend/default/other',
    #'service-1'=>'frontend/default/service_1',
    #'service-2'=>'frontend/default/service_2',
    #'service-3'=>'frontend/default/service_3',
    #'service-4'=>'frontend/default/service_4',
    'sendemail'=>'frontend/default/sendemail',
);